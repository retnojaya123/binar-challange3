const express = require('express');
const app = express();
const port = 8080;
const user = require("./user.json")
const fs = require("fs");


app.use(express.json());
app.use(express.urlencoded({ extended: true}));
app.set('views', './views');
app.set('view engine', 'ejs');
app.use(express.static("public"));
app.post("/register",  (req, res) => {
    const {username,password,name} = req.body
    const check = user.find((test) => test.username === username);
    if (username === "" || password === "" || name === "" ) {
        res.status(400);
        res.send("data belum terisi dengan lengkap");
    }else if (check) {
        res.status(409);
        res.send(`${username} sudah ada`);
      }
    else  {
        const id = user[user.length - 1].id + 1
        const users = {
            id,username,password,name
        }
        user.push(users)
        fs.writeFileSync("./user.json", JSON.stringify(user));
        res.status(200).json(users)
    }
})
app.post("/login",  (req, res) => {
    let username  = req.body.username;
    let password = req.body.password;
    const check = user.find((test) => test.username === username && test.password === password);
    if(check) {
      res.redirect("/index");
      res.status(200);
    }else if(username==="" || password===""){
      res.status(400);
      res.send("username atau password belum ter isi") 
    }else {
      res.status(401);
      res.send("tidak tersedia di database")
    }
})

app.get("/index", (req, res) => {
    res.render("index");
})
app.get("/game", (req, res) => {
    res.render("game");
})

app.listen(port, () => console.log(`server running on port ${port}`))